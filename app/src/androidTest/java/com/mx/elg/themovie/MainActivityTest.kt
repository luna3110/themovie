package com.mx.elg.themovie

import androidx.test.espresso.IdlingRegistry
import com.jakewharton.espresso.OkHttp3IdlingResource
import junit.framework.TestCase
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeUnit

/**
 * @author Edith Luna Galván
 * @creationDate 23, May, 2021
 * @modificationDate 23, May, 2021
 */

class MainActivityTest : TestCase() {

    private val mockWebServer = MockWebServer()


    @Before
    fun setup() {
        val okHttpClient = OkHttpClient.Builder ()
            .connectTimeout (2, TimeUnit.SECONDS)
            .readTimeout (2, TimeUnit.SECONDS)
            .writeTimeout (2, TimeUnit.SECONDS)
            .build ()

        mockWebServer.start(8080)
        IdlingRegistry.getInstance().register(
            OkHttp3IdlingResource.create("okhttp", okHttpClient)
        )
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()
    }

    @Test
    fun getListPopularMovie() {
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(200)
            .setBody(FileReader.readStringFromFile("success_response_popular.json")))
    }

    @Test
    fun getListTopRatedMovie() {
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(200)
            .setBody(FileReader.readStringFromFile("succes_response_top_rated.json")))
    }

    @Test
    fun getUrlVideo() {
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(200)
            .setBody(FileReader.readStringFromFile("succes_response_url_video.json")))
    }

    @Test
    fun testFailedResponse() {
        mockWebServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return MockResponse().throttleBody(1024, 5, TimeUnit.SECONDS)
            }
        }
    }
}