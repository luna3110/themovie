package com.mx.elg.themovie.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.mx.elg.themovie.R
import com.mx.elg.themovie.databinding.AnimationLottieBinding

/**
 * @creationDate        10, May, 2021
 * @modificationDate    10, May, 2021
 * @author              Edith Luna Galván
 */

class DialogLottie : DialogFragment()
{
    var fragmentMG : FragmentManager? = null
    private val lottieAsset = "animation_loading.json"

    private lateinit var binding : AnimationLottieBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme_FullScreenDialog_Transparent)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = AnimationLottieBinding.inflate(inflater, container, true)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isCancelable = false

        binding.animationView.imageAssetsFolder = "assets"
        binding.animationView.setAnimation(lottieAsset)
        binding.animationView.playAnimation()
    }

    override fun onStart() {
        super.onStart()
        dialog!!.window.apply {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog!!.window!!.setLayout(width, height)
        }
    }

    companion object
    {
        const val TAG = "DialogLottie"

        fun display(fragmentManager: FragmentManager): DialogLottie
        {
            val dialogFragment = DialogLottie()

            dialogFragment.show(fragmentManager, TAG)
            dialogFragment.fragmentMG = fragmentManager
            return dialogFragment
        }
    }
}
