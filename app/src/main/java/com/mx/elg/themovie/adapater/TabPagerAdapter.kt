package com.mx.elg.themovie.adapater

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

/**
 * @creationDate        19, May, 2021
 * @modificationDate    19, May, 2021
 * @author              Edith Luna Galván
 */

class TabPagerAdapter(fragmentManager : FragmentManager, private val bundle: Bundle?, private val fragments : ArrayList<Fragment>, private val tabTitles : ArrayList<String>): FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position : Int) : Fragment
    {
        val fragment =   fragments[position]
        fragment.arguments = bundle
        return fragment
    }

    override fun getCount() : Int
    {
        return fragments.size
    }

    override fun getPageTitle(position : Int) : CharSequence?
    {
        return tabTitles[position]
    }
}
