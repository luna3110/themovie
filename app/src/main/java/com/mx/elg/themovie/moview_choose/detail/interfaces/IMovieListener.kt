package com.mx.elg.themovie.moview_choose.detail.interfaces

import com.mx.elg.themovie.core.model.Movie

/**
 * @creationDate        21, May, 2021
 * @modificationDate    21, May, 2021
 * @author              Edith Luna Galván
 */

interface IMovieListener
{
    fun onMovieDetail(movie : Movie.Results)
}