package com.mx.elg.themovie

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mx.elg.themovie.moview_choose.MovieChooseFragment

class MainActivity : AppCompatActivity()
{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val newFragment = MovieChooseFragment()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.content_fragment, newFragment)
        transaction.commit()
    }
}