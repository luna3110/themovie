package com.mx.elg.themovie.core

import com.mx.elg.themovie.core.model.Movie
import com.mx.elg.themovie.core.model.Video
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

/**
 * @creationDate        20, May, 2021
 * @modificationDate    20, May, 2021
 * @author              Edith Luna Galván
 */

interface IAPIService
{
    @GET("popular")
    fun getListMoviewPopular(@Query("api_key") api_key: String?,
                              @Query("language") language: String?,
                              @Query("page") page: Int?) : Call<Movie?>?

    @GET("top_rated")
    fun getListMoviewTopRater(@Query("api_key") api_key: String?,
                             @Query("language") language: String?,
                             @Query("page") page: Int?) : Call<Movie?>?


    @GET
    fun getVideoMovie(@Url url : String,
                      @Query("api_key") api_key: String?,
                      @Query("language") language: String?) : Call<Video?>?
}