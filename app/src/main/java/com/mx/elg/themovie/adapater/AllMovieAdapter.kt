package com.mx.elg.themovie.adapater

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.mx.elg.themovie.R
import com.mx.elg.themovie.core.model.Movie.*
import com.mx.elg.themovie.databinding.ItemMovieBinding
import com.mx.elg.themovie.moview_choose.movie_find.fragment.MovieFindFragment
import com.mx.elg.themovie.moview_choose.detail.interfaces.IMovieListener
import com.squareup.picasso.Picasso
import java.util.*

/**
 * @creationDate        22, May, 2021
 * @modificationDate    22, May, 2021
 * @author              Edith Luna Galván
 */

class AllMovieAdapter(var data: MutableList<Results>, var contextPopular : MovieFindFragment, var listener : IMovieListener) : RecyclerView.Adapter<AllMovieAdapter.ViewHolder>()
{
    var filterData: List<Results> = data

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemMovieBinding>(inflater, R.layout.item_movie, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = filterData.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(filterData[position])
    }

    inner class ViewHolder(val binding: ItemMovieBinding): RecyclerView.ViewHolder(binding.root)
    {
        private val baseUrl = "https://image.tmdb.org/t/p/w500"
        fun bind(item: Results) {
            binding.movie = item
            binding.movieListener = listener

            Picasso.with(contextPopular.context)
                    .load(baseUrl + item.poster_path)
                    .into(binding.imageView)

            binding.executePendingBindings()
        }
    }

    fun filter(newString: String) {
        val localLoc = Locale("es", "MX")

        val auxFilter = newString.toUpperCase(localLoc)
        filterData =
            if (auxFilter.isEmpty() || auxFilter.isBlank())
                data
            else
                data.filter {
                    it.original_title!!.toUpperCase(localLoc).contains(auxFilter)
                }

        notifyDataSetChanged()
    }

    fun updateDataSet(newData: MutableList<Results>) {
        this.data = newData
        this.filterData = this.data
        notifyDataSetChanged()
    }
}