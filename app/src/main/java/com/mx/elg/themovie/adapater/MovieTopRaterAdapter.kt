package com.mx.elg.themovie.adapater

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.mx.elg.themovie.R
import com.mx.elg.themovie.databinding.ItemMovieBinding
import com.mx.elg.themovie.core.model.Movie
import com.mx.elg.themovie.moview_choose.detail.interfaces.IMovieListener
import com.mx.elg.themovie.moview_choose.movie_top_rater.fragment.MovieTopRaterFragment
import com.squareup.picasso.Picasso

/**
 * @creationDate        20, May, 2021
 * @modificationDate    20, May, 2021
 * @author              Edith Luna Galván
 */

class MovieTopRaterAdapter(var data: MutableList<Movie.Results>, var contextTopRated : MovieTopRaterFragment, var listener : IMovieListener) : RecyclerView.Adapter<MovieTopRaterAdapter.ViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemMovieBinding>(inflater, R.layout.item_movie, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position])
    }

    inner class ViewHolder(val binding: ItemMovieBinding): RecyclerView.ViewHolder(binding.root)
    {
        private val baseUrl = "https://image.tmdb.org/t/p/w500"
        fun bind(item: Movie.Results) {
            binding.movie = item
            binding.movieListener = listener

            Picasso.with(contextTopRated.context)
                    .load(baseUrl + item.poster_path)
                    .into(binding.imageView)


            binding.executePendingBindings()
        }
    }

    fun updateDataSet(newData: MutableList<Movie.Results>) {
        this.data = newData
        notifyDataSetChanged()
    }
}