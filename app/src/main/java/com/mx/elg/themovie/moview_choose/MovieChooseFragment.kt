package com.mx.elg.themovie.moview_choose

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mx.elg.themovie.R
import com.mx.elg.themovie.adapater.TabPagerAdapter
import com.mx.elg.themovie.databinding.FragmentChooseBinding
import com.mx.elg.themovie.moview_choose.movie_find.fragment.MovieFindFragment
import com.mx.elg.themovie.moview_choose.movie_popular.fragment.MoviePopularFragment
import com.mx.elg.themovie.moview_choose.movie_top_rater.fragment.MovieTopRaterFragment

/**
 * @creationDate        19, May, 2021
 * @modificationDate    19, May, 2021
 * @author              Edith Luna Galván
 */

class MovieChooseFragment : Fragment(), View.OnClickListener
{
    private lateinit var binding : FragmentChooseBinding
    private lateinit var tabPagerAdapter : TabPagerAdapter


    override fun onCreateView(inflater : LayoutInflater, container : ViewGroup?, savedInstanceState : Bundle?) : View? {
        binding = FragmentChooseBinding.inflate(inflater, container, false).also {
            it.lifecycleOwner = this
        }
        return binding.root
    }

    override fun onViewCreated(view : View, savedInstanceState : Bundle?)
    {
        val listFragments = ArrayList<Fragment>()
        listFragments.add(MoviePopularFragment())
        listFragments.add(MovieTopRaterFragment())

        val lisTitles = ArrayList<String>()
        lisTitles.add(getString(R.string.title_popular))
        lisTitles.add(getString(R.string.title_top_rated))

        binding.listener = this

        tabPagerAdapter = TabPagerAdapter(fragmentManager = childFragmentManager, bundle =  arguments, fragments = listFragments, tabTitles =  lisTitles)
        binding.pagerAdapter = tabPagerAdapter
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            binding.find.id -> {
                val newFragment =
                    MovieFindFragment()

                val transaction = requireActivity().supportFragmentManager.beginTransaction()
                transaction.replace(R.id.content_fragment, newFragment).addToBackStack(tag)
                transaction.commit()
            }
        }
    }
}

