package com.mx.elg.themovie.moview_choose.movie_top_rater.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.mx.elg.themovie.MainActivity
import com.mx.elg.themovie.R
import com.mx.elg.themovie.core.IAPIService
import com.mx.elg.themovie.core.implementations.MoviePresenter
import com.mx.elg.themovie.core.model.Movie
import com.mx.elg.themovie.databinding.FragmentTopRaterBinding
import com.mx.elg.themovie.moview_choose.MovieViewModel
import com.mx.elg.themovie.moview_choose.detail.interfaces.IMovieListener
import com.mx.elg.themovie.moview_choose.detail.fragment.MovieDetailFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @creationDate        19, May, 2021
 * @modificationDate    19, May, 2021
 * @author              Edith Luna Galván
 */

class MovieTopRaterFragment : Fragment(), IMovieListener
{
    private lateinit var binding : FragmentTopRaterBinding
    private lateinit var viewModelMovie : MovieViewModel

    override fun onCreateView(inflater : LayoutInflater, container : ViewGroup?, savedInstanceState : Bundle?) : View? {
        binding = FragmentTopRaterBinding.inflate(inflater, container, false).also {
            it.lifecycleOwner = this
        }
        return binding.root
    }

    override fun onViewCreated(view : View, savedInstanceState : Bundle?)
    {
        initModels()
        viewModelMovie.init(this)
        binding.viewModel = viewModelMovie

        if (viewModelMovie.movieTopRaterList.value!!.size > 0)
            viewModelMovie.setItemsDataTopRater(viewModelMovie.movieTopRaterList.value!!)
        else{
            MoviePresenter.newInstance().getListTopRatedMovie(this,1, viewModelMovie)
        }
    }

    override fun onMovieDetail(movie: Movie.Results) {
        val newFragment =
            MovieDetailFragment()

        val bundle = Bundle()
        bundle.putSerializable("movie", movie)

        newFragment.arguments = bundle
        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.replace(com.mx.elg.themovie.R.id.content_fragment, newFragment).addToBackStack(tag)
        transaction.commit()
    }

    private fun initModels() {
        viewModelMovie = activity?.run { ViewModelProviders.of(this)[MovieViewModel::class.java] } ?: throw Exception("Invalid Activity")
    }
}