package com.mx.elg.themovie.adapater

import android.view.View
import androidx.databinding.BindingAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.mx.elg.themovie.moview_choose.detail.fragment.OneClickListener

/**
 * @creationDate        19, May, 2021
 * @modificationDate    19, May, 2021
 * @author              Edith Luna Galván
 */

@BindingAdapter("bind_viewPager")
fun setWithViewPager(tabLayout : TabLayout, pager : ViewPager) {
    tabLayout.setupWithViewPager(pager)
}

@BindingAdapter("bind_oneClick")
fun setOnClickListener(view : View, f : View.OnClickListener?) {
    when(f) {
        null -> view.setOnClickListener(null)
        else -> view.setOnClickListener(
            OneClickListener(
                f
            )
        )
    }
}
