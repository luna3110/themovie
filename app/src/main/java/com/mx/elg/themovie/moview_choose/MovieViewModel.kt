package com.mx.elg.themovie.moview_choose

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mx.elg.themovie.adapater.AllMovieAdapter
import com.mx.elg.themovie.adapater.MoviePopularAdapter
import com.mx.elg.themovie.adapater.MovieTopRaterAdapter
import com.mx.elg.themovie.core.model.Movie
import com.mx.elg.themovie.core.model.Video
import com.mx.elg.themovie.moview_choose.movie_find.fragment.MovieFindFragment
import com.mx.elg.themovie.moview_choose.movie_popular.fragment.MoviePopularFragment
import com.mx.elg.themovie.moview_choose.movie_top_rater.fragment.MovieTopRaterFragment

/**
 * @creationDate        20, May, 2021
 * @modificationDate    20, May, 2021
 * @author              Edith Luna Galván
 */

class MovieViewModel : ViewModel()
{
    private val _apiKey = MutableLiveData<String>()
    private val _lenguage = MutableLiveData<String>()
    private val _movieResult = MutableLiveData<Movie.Results>()
    private val _moviePopularList = MutableLiveData<MutableList<Movie.Results>>()
    private val _moviePopularAdapter = MutableLiveData<MoviePopularAdapter>()
    private val _movieTopRaterList = MutableLiveData<MutableList<Movie.Results>>()
    private val _movieTopRaterAdapter = MutableLiveData<MovieTopRaterAdapter>()
    private val _videoResult = MutableLiveData<Video.ResultVideo>()
    private val _allMovies = MutableLiveData<MutableList<Movie.Results>>()
    private val _allMovieAdapter = MutableLiveData<AllMovieAdapter>()


    val movieResult: LiveData<Movie.Results>
        get() = _movieResult

    val videoResult: LiveData<Video.ResultVideo>
        get() = _videoResult

    val moviePopularList: LiveData<MutableList<Movie.Results>>
        get() = _moviePopularList

    val movieTopRaterList: LiveData<MutableList<Movie.Results>>
        get() = _movieTopRaterList

    val moviePopularAdapter : LiveData<MoviePopularAdapter>
        get() = _moviePopularAdapter

    val movieTopRaterAdapter : LiveData<MovieTopRaterAdapter>
        get() = _movieTopRaterAdapter

    val apiKey: LiveData<String>
        get() = _apiKey

    val lenguage: LiveData<String>
        get() = _lenguage

    val allMovies: LiveData<MutableList<Movie.Results>>
        get() = _allMovies

    val allMovieAdapter : LiveData<AllMovieAdapter>
        get() = _allMovieAdapter


    init {
        _apiKey.value = "f1cca3de1acdb7a36b961de832f21eb3"
        _lenguage.value = "en-US"
        _moviePopularList.value =  mutableListOf()
        _movieTopRaterList.value = mutableListOf()
        _allMovies.value = mutableListOf()
        _movieResult.value = null
        _videoResult.value = null
    }

    fun init(listener: MoviePopularFragment) {
        _moviePopularAdapter.value = MoviePopularAdapter(mutableListOf(), listener, listener)
    }

    fun init(listener: MovieTopRaterFragment) {
        _movieTopRaterAdapter.value = MovieTopRaterAdapter(mutableListOf(), listener, listener)
    }

    fun init(listener: MovieFindFragment) {
        _allMovieAdapter.value = AllMovieAdapter(arrayListOf(), listener, listener)
    }

    fun setItemsDataPopular(data : MutableList<Movie.Results>) {
        _moviePopularList.value = data
        _moviePopularAdapter.value!!.updateDataSet(moviePopularList.value!!)
    }

    fun setItemsDataTopRater(data : MutableList<Movie.Results>) {
        _movieTopRaterList.value = data
        _movieTopRaterAdapter.value!!.updateDataSet(movieTopRaterList.value!!)
    }

    fun setItemsAllMovies(){
        _allMovies.value!!.clear()
        _allMovies.value!!.addAll(moviePopularList.value!!)
        _allMovies.value!!.addAll(_movieTopRaterList.value!!)
        _allMovieAdapter.value!!.updateDataSet(allMovies.value!!)
    }

    fun setMovie(data : Movie.Results){
        _movieResult.value = data
    }

    fun setVideoResult(data : Video.ResultVideo){
        _videoResult.value = data
    }
}