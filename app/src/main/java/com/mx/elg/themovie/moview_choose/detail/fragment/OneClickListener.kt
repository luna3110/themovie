package com.mx.elg.themovie.moview_choose.detail.fragment

import android.os.SystemClock
import android.view.View

/**
 * @creationDate        22, May, 2021
 * @modificationDate    22, May, 2021
 * @author              Edith Luna Galván
 */

class OneClickListener(private val onClickListener: View.OnClickListener) : View.OnClickListener {

    private var debounceInterval: Long = 500
    private var lastClickTime: Long = 0

    override fun onClick(v: View?) {
        if(SystemClock.elapsedRealtime() - lastClickTime < debounceInterval) {
            return
        }
        lastClickTime = SystemClock.elapsedRealtime()
        onClickListener.onClick(v)
    }
}