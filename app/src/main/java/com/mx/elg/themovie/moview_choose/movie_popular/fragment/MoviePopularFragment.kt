package com.mx.elg.themovie.moview_choose.movie_popular.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.mx.elg.themovie.core.implementations.MoviePresenter
import com.mx.elg.themovie.core.model.Movie
import com.mx.elg.themovie.databinding.FragmentPopularBinding
import com.mx.elg.themovie.moview_choose.MovieViewModel
import com.mx.elg.themovie.moview_choose.detail.interfaces.IMovieListener
import com.mx.elg.themovie.moview_choose.detail.fragment.MovieDetailFragment

/**
 * @creationDate        19, May, 2021
 * @modificationDate    19, May, 2021
 * @author              Edith Luna Galván
 */

class MoviePopularFragment : Fragment(), IMovieListener
{
    private lateinit var binding : FragmentPopularBinding
    private lateinit var viewModelMovie : MovieViewModel

    override fun onCreateView(inflater : LayoutInflater, container : ViewGroup?, savedInstanceState : Bundle?) : View? {
        binding = FragmentPopularBinding.inflate(inflater, container, false).also {
            it.lifecycleOwner = this
        }
        return binding.root
    }

    override fun onViewCreated(view : View, savedInstanceState : Bundle?)
    {
        initModels()
        viewModelMovie.init(this)
        binding.viewModel = viewModelMovie

        viewModelMovie.init(this)
        if (viewModelMovie.moviePopularList.value!!.size > 0)
            viewModelMovie.setItemsDataPopular(viewModelMovie.moviePopularList.value!!)
        else{
            MoviePresenter.newInstance().getListPopularMovie(this,1, viewModelMovie)
        }
    }

    override fun onMovieDetail(movie: Movie.Results) {
        val newFragment = MovieDetailFragment()

        val bundle = Bundle()
        bundle.putSerializable("movie", movie)

        newFragment.arguments = bundle
        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.replace(com.mx.elg.themovie.R.id.content_fragment, newFragment).addToBackStack(tag)
        transaction.isAddToBackStackAllowed
        transaction.commit()
    }

    private fun initModels() {
        viewModelMovie = activity?.run { ViewModelProviders.of(this)[MovieViewModel::class.java] } ?: throw Exception("Invalid Activity")
    }
}

