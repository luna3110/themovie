package com.mx.elg.themovie.core.model

import com.google.gson.GsonBuilder
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * @creationDate        22, May, 2021
 * @modificationDate    22, May, 2021
 * @author              Edith Luna Galván
 */
class Video
{
    @SerializedName("id")
    @Expose
    var id: Int? = 0

    @SerializedName("results")
    @Expose
    var results: MutableList<ResultVideo>? = null

    class ResultVideo : Serializable
    {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("iso_639_1")
        @Expose
        var iso_639_1: String? = null

        @SerializedName("iso_3166_1")
        @Expose
        var iso_3166_1: String? = null

        @SerializedName("key")
        @Expose
        var key: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("site")
        @Expose
        var site: String? = null

        @SerializedName("size")
        @Expose
        var size: Int? = 0

        @SerializedName("type")
        @Expose
        var type: String? = null
    }

    override fun toString(): String {
        return GsonBuilder().disableHtmlEscaping().create().toJson(this)
    }
}