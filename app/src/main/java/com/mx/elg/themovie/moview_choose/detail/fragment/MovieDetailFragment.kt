package com.mx.elg.themovie.moview_choose.detail.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.mx.elg.themovie.MainActivity
import com.mx.elg.themovie.core.implementations.MoviePresenter
import com.mx.elg.themovie.core.model.Movie
import com.mx.elg.themovie.databinding.FragmentDetailMovieBinding
import com.mx.elg.themovie.moview_choose.MovieViewModel
import com.squareup.picasso.Picasso


/**
 * @creationDate        21, May, 2021
 * @modificationDate    21, May, 2021
 * @author              Edith Luna Galván
 */

@SuppressLint("SetJavaScriptEnabled")
class MovieDetailFragment : Fragment(), View.OnClickListener
{
    private lateinit var binding: FragmentDetailMovieBinding

    private lateinit var viewModelMovie : MovieViewModel

    override fun onCreateView(inflater : LayoutInflater, container : ViewGroup?, savedInstanceState : Bundle?) : View? {
        binding = FragmentDetailMovieBinding.inflate(inflater, container, false).also {
            it.lifecycleOwner = this
        }
        return binding.root
    }
    override fun onViewCreated(view : View, savedInstanceState : Bundle?)
    {
        initModels()
        val argumentsFragment = arguments
        viewModelMovie.setMovie(argumentsFragment!!.getSerializable("movie") as Movie.Results)
        binding.movie = viewModelMovie.movieResult.value!!
        binding.listener = this

        val baseUrl = "https://image.tmdb.org/t/p/w500"

        Picasso.with(context)
                .load(baseUrl + viewModelMovie.movieResult.value!!.poster_path)
                .into(binding.imageView)

        MoviePresenter.newInstance().getUrlVideo(this,viewModelMovie.movieResult.value!!.id!!, viewModelMovie)


    }

    override fun onClick(v: View?) {
        when(v!!.id){
            binding.btnBack.id -> {
                (activity as MainActivity).onBackPressed()
            }
        }
    }

    fun showVideoResult(enabled : Boolean){
        if (enabled) showVideo() else binding.webTrailer.visibility = View.GONE
    }

    fun showVideo() {
        binding.webTrailer.visibility = View.VISIBLE
        val url = "https://www.youtube.com/embed/" + viewModelMovie.videoResult.value!!.key + "?autoplay=1&vq=small"
        binding.webTrailer.settings.javaScriptEnabled = true
        binding.webTrailer.stopLoading()
        binding.webTrailer.loadUrl(url)
    }



    private fun initModels() {
        viewModelMovie = activity?.run { ViewModelProviders.of(this)[MovieViewModel::class.java] } ?: throw Exception("Invalid Activity")
    }
}