package com.mx.elg.themovie.core.interfaces

import androidx.fragment.app.Fragment
import com.mx.elg.themovie.moview_choose.MovieViewModel

/**
 * @creationDate        23, May, 2021
 * @modificationDate    23, May, 2021
 * @author              Edith Luna Galván
 * @company             Banco Azteca
 * @since               1.0
 */
interface IMovieTMBDBPresenter
{

    fun getListPopularMovie(context: Fragment, page : Int, viewModel: MovieViewModel)

    fun getListTopRatedMovie(context: Fragment, page : Int, viewModel: MovieViewModel)

    fun getUrlVideo(context: Fragment, id : Int, viewModel: MovieViewModel)
}