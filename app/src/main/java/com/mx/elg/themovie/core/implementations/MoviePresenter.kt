package com.mx.elg.themovie.core.implementations

import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.mx.elg.themovie.R
import com.mx.elg.themovie.core.IAPIService
import com.mx.elg.themovie.core.interfaces.IMovieTMBDBPresenter
import com.mx.elg.themovie.core.model.Movie
import com.mx.elg.themovie.core.model.Video
import com.mx.elg.themovie.dialog.DialogLottie
import com.mx.elg.themovie.moview_choose.MovieViewModel
import com.mx.elg.themovie.moview_choose.detail.fragment.MovieDetailFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * @creationDate        22, May, 2021
 * @modificationDate    22, May, 2021
 * @author              Edith Luna Galván
 * @company             Banco Azteca
 * @since               1.0
 */

class MoviePresenter : IMovieTMBDBPresenter
{
    companion object{
        fun newInstance(): MoviePresenter {
            return MoviePresenter()
        }
    }

    var loadingDialog: DialogLottie?= null


    override fun getListPopularMovie(context: Fragment, page: Int, viewModel: MovieViewModel) {
        val service: IAPIService = getRetrofit().create(IAPIService::class.java)

        val responseCall: Call<Movie?> =
            service.getListMoviewPopular(viewModel.apiKey.value,viewModel.lenguage.value,1)!!

        loadingDialog = DialogLottie.display(context.childFragmentManager)
        responseCall.enqueue(object : Callback<Movie?> {
            override fun onResponse(call: Call<Movie?>, response: Response<Movie?>) {
                viewModel.setItemsDataPopular(response.body()!!.results!!)
                loadingDialog!!.dismiss()
            }

            override fun onFailure(call: Call<Movie?>, t: Throwable) {
                showDialogErrorNetwork(context)
                loadingDialog!!.dismiss()
            }
        })
    }

    override fun getListTopRatedMovie(context: Fragment, page: Int, viewModel: MovieViewModel) {
        val service: IAPIService = getRetrofit().create(IAPIService::class.java)

        val responseCall: Call<Movie?> =
            service.getListMoviewTopRater(viewModel.apiKey.value,viewModel.lenguage.value,2)!!

        loadingDialog = DialogLottie.display(context.childFragmentManager)
        responseCall.enqueue(object : Callback<Movie?> {
            override fun onResponse(call: Call<Movie?>, response: Response<Movie?>) {
                viewModel.setItemsDataTopRater(response.body()!!.results!!)
                loadingDialog!!.dismiss()
            }

            override fun onFailure(call: Call<Movie?>, t: Throwable) {
                showDialogErrorNetwork(context)
                loadingDialog!!.dismiss()
            }
        })
    }

    override fun getUrlVideo(context: Fragment, id: Int, viewModel: MovieViewModel) {
        val service: IAPIService = getRetrofit().create(IAPIService::class.java)

        val responseCall: Call<Video?> =
            service.getVideoMovie(viewModel.movieResult.value!!.id.toString() + "/videos",viewModel.apiKey.value,viewModel.lenguage.value)!!

        loadingDialog = DialogLottie.display(context.childFragmentManager)
        responseCall.enqueue(object : Callback<Video?> {
            override fun onResponse(call: Call<Video?>, response: Response<Video?>) {
                loadingDialog!!.dismiss()
                if (response.body()!!.results != null && response.body()!!.results!!.size > 0){
                    viewModel.setVideoResult(response.body()!!.results!!.get(0))
                    (context as MovieDetailFragment).showVideoResult(true)
                }else{
                    (context as MovieDetailFragment).showVideoResult(false)
                    showDialog(context)
                }
            }

            override fun onFailure(call: Call<Video?>, t: Throwable) {
                loadingDialog!!.dismiss()
                showDialogErrorNetwork(context)
            }
        })
    }

    private fun getRetrofit(): Retrofit {
    return Retrofit.Builder()
        .baseUrl("https://api.themoviedb.org/3/movie/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    }

    private fun showDialogErrorNetwork(fragment : Fragment){
        val alertDialog = AlertDialog.Builder(fragment.context!!)

        alertDialog.setTitle(fragment.context!!.getString(R.string.error_title))
            .setMessage(fragment.context!!.getString(R.string.error_msg))
            .setPositiveButton(fragment.context!!.getString(R.string.error_btn_accept)) { dialog, _ -> dialog.dismiss() }
            .setCancelable(true)
            .show()
    }

    private fun showDialog(fragment : Fragment)
    {
        val alertDialog = AlertDialog.Builder(fragment.context!!)

        alertDialog.setTitle(fragment.context!!.getString(R.string.not_find_video_title))
            .setMessage(fragment.context!!.getString(R.string.not_find_video_msg))
            .setPositiveButton(fragment.context!!.getString(R.string.error_btn_accept)) { dialog, _ -> dialog.dismiss() }
            .setCancelable(true)
            .show()
    }
}