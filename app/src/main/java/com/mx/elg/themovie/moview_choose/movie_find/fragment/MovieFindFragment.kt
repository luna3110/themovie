package com.mx.elg.themovie.moview_choose.movie_find.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.mx.elg.themovie.core.model.Movie
import com.mx.elg.themovie.databinding.FragmentFindBinding
import com.mx.elg.themovie.moview_choose.MovieViewModel
import com.mx.elg.themovie.moview_choose.detail.interfaces.IMovieListener
import com.mx.elg.themovie.moview_choose.detail.fragment.MovieDetailFragment


/**
 * @creationDate        22, May, 2021
 * @modificationDate    22, May, 2021
 * @author              Edith Luna Galván
 */
class MovieFindFragment : Fragment(), IMovieListener
{
    private lateinit var binding : FragmentFindBinding
    private lateinit var viewModelMovie : MovieViewModel

    override fun onCreateView(inflater : LayoutInflater, container : ViewGroup?, savedInstanceState : Bundle?) : View? {
        binding = FragmentFindBinding.inflate(inflater, container, false).also {
            it.lifecycleOwner = this
        }
        return binding.root
    }

    override fun onViewCreated(view : View, savedInstanceState : Bundle?)
    {
        initModels()
        viewModelMovie.init(this)
        binding.viewModel = viewModelMovie

        viewModelMovie.setItemsAllMovies()
        binding.searchMovie.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                viewModelMovie.allMovieAdapter.value!!.filter(newText ?: "")
                return false
            }

        })
    }

    override fun onMovieDetail(movie: Movie.Results) {
        val newFragment =
            MovieDetailFragment()

        val bundle = Bundle()
        bundle.putSerializable("movie", movie)

        newFragment.arguments = bundle
        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.replace(com.mx.elg.themovie.R.id.content_fragment, newFragment).addToBackStack(tag)
        transaction.commit()
    }

    private fun initModels() {
        viewModelMovie = activity?.run { ViewModelProviders.of(this)[MovieViewModel::class.java] } ?: throw Exception("Invalid Activity")
    }
}